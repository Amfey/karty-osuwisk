# -*- coding: utf-8 -*-

## trzeba pobrać wiersze i wstawić je do excela przed uruchomieniem iteracji!!!!!!!!!!!!!!!!!!!!!!!!!!

# Definiujemy funkcje kopiującą rzeczy do schowka
import os

def addToClipBoard(text):
    u_text = text.encode('cp1250')
    
    command = 'echo|set /p=' + u_text.strip() + '| clip'
    os.system(command)

import win32clipboard

# Otwieramy schowek
win32clipboard.OpenClipboard()

# Zapisujemy dane ze schowka
data = win32clipboard.GetClipboardData()

# Zamykamy schowek
win32clipboard.CloseClipboard()

# Dzielimy dane na linie
splitdata = data.splitlines()

# Pobieramy wiersze
dl_text = splitdata[5]
hmax_text = splitdata[1]
hmin_text = splitdata[3]
nachylenie_text = splitdata[13]
azimuth_text = splitdata[12]

# Dzielimy wiersze
dl_split = dl_text.split()
hmax_split = hmax_text.split()
hmin_split = hmin_text.split()
nach_split = nachylenie_text.split()
az_split = azimuth_text.split()

# Pobieramy wartości
dl_liczba = dl_split[2]
hmax_liczba = hmax_split[2]
hmin_liczba = hmin_split[2]
nach_liczba = nach_split[1]
az_liczba = az_split[1]

# Dzielimy liczby
dl_lista = dl_liczba.split(".")
dl = dl_lista[0]
hmax_lista = hmax_liczba.split(".")
hmax = hmax_lista[0]
hmin_lista = hmin_liczba.split(".")
hmin = hmin_lista[0]
nach_lista = nach_liczba.split(".")
nach = nach_lista[0]
az_lista = az_liczba.split(".")
az_st = az_lista[0]

# Usuwamy znak minus przed nachyleniem
nach=int(nach)
nach=abs(nach)
nach=str(nach)

# Usuwamy znak stopnia
az = az_st[0:(len(az_st)-1)]



# -*- coding: utf-8 -*-
import xlrd

# Otwieramy plik wiersz gdzie jest przechowywany numer wiersza
plik = open("wiersz.txt")
try:
    wiersz = plik.read()
finally:
    plik.close()


# podanie nazwy pliku deflautowo nazwa pliku to s.xls

nazwa_pliku = raw_input('Podaj nazwę pliku: ') or "s.xls"

# otwarcie pliku
book = xlrd.open_workbook(nazwa_pliku, encoding_override="utf-8")

# wczytujemy pierwszy arkusz
sheet = book.sheet_by_index(0)

# Podajemy nazwę wiersza który będziemy wklejać
rowIDstr = raw_input("Podaj numer wiersza z którego będziemy kopiować do schowka: ") or wiersz
rowID = int(rowIDstr)
rowID = rowID - 1
wiersz=rowIDstr

print len(sheet.row_values(rowID,0))
print range(len(sheet.row_values(rowID,0)))


i = 0

# Iterator
for wartosc in range(len(sheet.row_values(rowID,0))):
    # set clipboard data
    komorka = sheet.cell(rowID,wartosc)
    wart_kom = komorka.value
    
    
    
    if wartosc == 3:
        # Dodajemy długość do schowka
        addToClipBoard(dl)
        print 'Skopiowano dlugosc ' + dl
        raw_input('Nacisnij enter aby kontynuowac....')
        
        # Dodajemy hmax do schowka
        addToClipBoard(hmax)
        print 'Skopiowano hmax ' + hmax
        raw_input('Nacisnij enter aby kontynuowac....')
        
        # Dodajemy hmin do schowka
        addToClipBoard(hmin)
        print 'Skopiowano hmin ' + hmin
        raw_input('Nacisnij enter aby kontynuowac....')
        
        # Dodajemy nachylenie do schowka
        addToClipBoard(nach)
        print 'Skopiowano nachylenie ' + nach
        raw_input('Nacisnij enter aby kontynuowac....')
        
        # Dodajemy azymut do schowka
        addToClipBoard(az)
        print 'Skopiowano azymut '+ az
        raw_input('Nacisnij enter aby kontynuowac...')
    
    
    if type(wart_kom) == float:
        wart_int = int(wart_kom)
        wart_string = str(wart_int)
        # set clipboard data
        addToClipBoard(wart_string)
        
        tytul = sheet.cell(0,i)
        nast = sheet.cell(0,i+1)
        i=i+1
        print tytul.value, " ", wart_string, "Następne będzie ", nast.value
        raw_input("Press Enter to continue...")
    
    else:
        #print type(wart_kom)
    
        # set clipboard data
        addToClipBoard(wart_kom)
    
        tytul = sheet.cell(0,i)
        i=i+1
        print tytul.value, " ", wart_kom
        raw_input("Press Enter to continue...")

    print wartosc
  
    
# Dodajemy kolejny numer wiersza do pliku        
wiersz_int=int(wiersz)
wiersz_int += 1
wiersz = str(wiersz_int)

plik = open("wiersz.txt", "w")
try:
    plik.write(wiersz)
finally:
    plik.close()



